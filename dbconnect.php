<?php 

/* Credentials */
$db_server = "localhost";
$db_user = "root";
$db_pass = "";
$db_name = "app_auth";


/* Connection */
$conn = new mysqli($db_server, $db_user, $db_pass, $db_name);

/* If connection fails for some reason */
if ($conn->connect_error) {
	die("Database connection failed: ". $conn->connect_error);
}

?>