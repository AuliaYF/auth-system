-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2017 at 05:01 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_auth`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_user`
--

CREATE TABLE `app_user` (
  `username` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `loginCode` varchar(255) NOT NULL,
  `status` char(1) NOT NULL,
  `lastIP` varchar(45) NOT NULL,
  `lastLogin` datetime NOT NULL,
  `createdTime` datetime NOT NULL,
  `updatedTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_user_recovery`
--

CREATE TABLE `app_user_recovery` (
  `idRecovery` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `recoveryCode` varchar(255) NOT NULL,
  `createdTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_user_verification`
--

CREATE TABLE `app_user_verification` (
  `idVerification` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `verificationCode` varchar(255) NOT NULL,
  `createdTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_user`
--
ALTER TABLE `app_user`
  ADD PRIMARY KEY (`username`,`email`);

--
-- Indexes for table `app_user_recovery`
--
ALTER TABLE `app_user_recovery`
  ADD PRIMARY KEY (`idRecovery`);

--
-- Indexes for table `app_user_verification`
--
ALTER TABLE `app_user_verification`
  ADD PRIMARY KEY (`idVerification`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_user_recovery`
--
ALTER TABLE `app_user_recovery`
  MODIFY `idRecovery` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_user_verification`
--
ALTER TABLE `app_user_verification`
  MODIFY `idVerification` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
