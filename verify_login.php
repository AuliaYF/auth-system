<?php
require ('AuthLib.php');
$auth = new AuthLib();

if(isset($_POST['submit'])){
	$user['id'] = $_GET['id'];
	$user['code'] = trim($_POST['code']);
	$res = $auth->doVerifyDevice($user['id'], $user['code']);

	if($res['error'] === FALSE){
		header('Location: index.php');
	}else {
		die($res['message']);
	}
}
?>
<form action="verify_login.php?id=<?php echo $_GET['id'] ?>" method="POST">
	<input type="text" name="code">
	<input type="submit" name="submit" value="Verify">
</form>