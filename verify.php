<?php
require ('AuthLib.php');
$auth = new AuthLib();

$res = $auth->doVerifyUser($_GET['id'], trim($_GET['code']));
if($res['error'] === FALSE){
	echo $res['message'];
	header('Refresh: 3; URL=login.php');
}else {
	die($res['message']);
}
