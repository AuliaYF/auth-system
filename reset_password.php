<?php
require ('AuthLib.php');
$auth = new AuthLib();
if(isset($_POST['submit'])){
	$res = $auth->doResetPassword($_GET['id'], trim($_GET['code']), trim($_POST['password']));

	if($res['error'] === FALSE){
		echo $res['message'];
		header('Refresh: 3; URL=login.php');
	}else {
		echo $res['message'];
	}
}
?>
<form action="reset_password.php?id=<?php echo $_GET['id'] ?>&code=<?php echo $_GET['code'] ?>" method="POST">
	<input type="password" name="password">
	<input type="submit" name="submit" value="Reset">
	<br>
</form>