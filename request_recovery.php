<?php
require ('AuthLib.php');
$auth = new AuthLib();
if(isset($_POST['submit'])){
	$res = $auth->doRequestPasswordRecoveryCode($_POST['username']);

	if($res['error'] === FALSE){
		echo "We've sent an email to you.";
	}else {
		echo $res['message'];
	}
}
?>
<form action="request_recovery.php" method="POST">
	<input type="text" name="username">
	<input type="submit" name="submit" value="Request">
	<br>
</form>