<?php
/**
 * Email
 * @author Aulia Yusuf Fachrezy
 */
class Email
{
    /**
     * Config
     */
    private $host = "";
    private $port = "";
    private $username = "";
    private $password = "";

    private $sentFromEmail = "test@local.com";
    private $sentFromName = "PHPMailer Tester";

    public function sendEmail($destination, $subject, $body){
    	require './PHPMailer/PHPMailerAutoload.php';

    	$mail = new PHPMailer;

    	$mail->isSMTP();
    	$mail->SMTPDebug = 0;
    	$mail->Debugoutput = 'html';
    	$mail->Host = $this->host;
    	$mail->Port = $this->port;
    	$mail->SMTPSecure = 'tls';
    	$mail->SMTPAuth = true;
    	$mail->Username = $this->username;
    	$mail->Password = $this->password;
    	$mail->setFrom($this->sentFromEmail, $this->sentFromName);
    	$mail->addAddress($destination);
    	$mail->Subject = $subject;
    	$mail->Body    = $body;
    	$mail->isHTML(true);

    	if (!$mail->send()) {
    		return array('error' => TRUE, 'message' => $mail->ErrorInfo);
    	} else {
    		return array('error' => FALSE, 'message' => 'OK');
    	}
    }
}