<?php
require ('AuthLib.php');
$auth = new AuthLib();
if(isset($_POST['submit'])){
	$user['username'] = $_POST['username'];
	$user['password'] = trim($_POST['password']);
	$user['ipAddress'] = $_SERVER['REMOTE_ADDR']; 
	$res = $auth->doLogin($user);

	if($res['error'] === FALSE){
		header('Location: index.php');
	}else {
		if(isset($res['needVerification']))
			header('Location: verify_login.php?id=' . $user['username']);
		else
			echo $res['message'];
	}
}
?>
<form action="login.php" method="POST">
	<input type="text" name="username">
	<input type="password" name="password">
	<input type="submit" name="submit" value="Login">
	<br>
	<a href="request_recovery.php">Forgot Password?</a>
</form>