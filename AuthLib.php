<?php

/**
 * AuthLib
 * Handles user creation, password verification, password recovery, user login verification
 * @author AuliaYF
 */
class AuthLib{
	/*
	User Status
	p => pending or waiting for verification
	t => activated
	f => deactivated or blacklisted
	r => recovery state / password reset
	*/

	private $status = array(
		'p' => 'verification',
		't' => 'activated',
		'f' => 'blacklisted',
		'r' => 'recovery'
	);

	public function __construct(){
		session_start();
	}

	public function doCreateUser($userData = NULL){
		if(NULL === $userData)
			return array('error' => TRUE, 'message' => 'User Data cannot be empty.');

		$dateTime = date("Y-m-d H:i:s");

		require ('dbconnect.php');

        // Check if username or email is already taken.
		$stmt = $conn->prepare("SELECT username FROM app_user WHERE email = ? OR username = ?");
		$stmt->bind_param("ss", $userData['email'], $userData['username']);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();

		if($result->num_rows != 0)
			return array('error' => TRUE, 'message' => 'Username/Email already exists.');

		$userData['password'] = password_hash(trim($userData['password']), PASSWORD_DEFAULT);
		$userData['status'] = "p";
		$userData['createdTime'] = $dateTime;

		// Add to app_user
		$stmt = $conn->prepare("INSERT INTO app_user (username, email, password, status, createdTime) VALUES (?, ?, ?, ?, ?)");
		$stmt->bind_param("sssss", $userData['username'], $userData['email'], $userData['password'], $userData['status'], $dateTime);
		$stmt->execute();
		$stmt->close();

		// Add to app_user_verification
		$verificationCode = substr(md5(mt_rand()),0,15);
		$stmt = $conn->prepare("INSERT INTO app_user_verification (username, verificationCode, createdTime) VALUES (?, ?, ?)");
		$stmt->bind_param("sss", $userData['username'], $verificationCode, $dateTime);
		$stmt->execute();
		$stmt->close();

		require ('Email.php');
		$mail = new Email;
		$body='Your verification code is: '.$verificationCode.'<br> To activate your account please click on the following link <a href="http://localhost/project/auth-system/verify.php?id='.$userData['username'].'&code='.$verificationCode.'">verify.php?id='.$userData['username'].'&code='.$verificationCode.'</a>.';
		$mail->sendEmail($userData['email'], 'Your activation code for Membership.', $body);
		return array('error' => FALSE, 'message' => ('OK. ' . $verificationCode));
	}

	public function doVerifyUser($username = NULL, $verificationCode = NULL){
		if((!isset($username) || trim($username)===''))
			return array('error' => TRUE, 'message' => 'Username cannot be empty.');

		if((!isset($verificationCode) || trim($verificationCode)===''))
			return array('error' => TRUE, 'message' => 'Verification Code cannot be empty.');

		require ('dbconnect.php');
		$stmt = $conn->prepare("SELECT username FROM app_user_verification WHERE verificationCode = ?");
		$stmt->bind_param("s", $verificationCode);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();

		if($result->num_rows != 0){
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			if($row['username'] === $username){
				$status = "t"; // Activated
				$dateTime = date("Y-m-d H:i:s");

				$stmt = $conn->prepare("UPDATE app_user SET status = ?, updatedTime = ? WHERE username = ?");
				$stmt->bind_param("sss", $status, $dateTime, $username);
				$stmt->execute();
				$stmt->close();

				$stmt = $conn->prepare("DELETE FROM app_user_verification WHERE username = ? AND verificationCode = ?");
				$stmt->bind_param("ss", $username, $verificationCode);
				$stmt->execute();
				$stmt->close();

				return array('error' => FALSE, 'message' => 'User Verification success, you can login now.');
			}
		}
		return array('error' => TRUE, 'message' => 'Invalid Verification Code.');
	}

	public function doLogin($userData = NULL){
		if(NULL === $userData)
			return array('error' => TRUE, 'message' => 'User Data cannot be empty.');

		require ('dbconnect.php');
		$stmt = $conn->prepare("SELECT username, password, email, lastIP, status FROM app_user WHERE username = ?");
		$stmt->bind_param("s", $userData['username']);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();

		if($result->num_rows != 0){
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			if(password_verify($userData['password'], $row['password'])){
				$dateTime = date("Y-m-d H:i:s");
				if($row['status'] !== "t")
					return array('error' => TRUE, 'message' => 'User status: ' . strtoupper($this->status[$row['status']]));

				if($userData['ipAddress'] === $row['lastIP']){
					$stmt = $conn->prepare("UPDATE app_user SET lastIP = ?, lastLogin = ?, loginCode = NULL, updatedTime = ? WHERE username = ?");
					$stmt->bind_param("ssss", $userData['ipAddress'], $dateTime, $dateTime, $userData['username']);
					$stmt->execute();
					$stmt->close();

					$_SESSION['user_id'] = $row['username'];
					return array('error' => FALSE, 'message' => 'OK.');
				}else{
					$loginCode = substr(md5(mt_rand()),0,15);

					$stmt = $conn->prepare("UPDATE app_user SET loginCode = ?, updatedTime = ? WHERE username = ?");
					$stmt->bind_param("sss", $loginCode, $dateTime, $userData['username']);
					$stmt->execute();
					$stmt->close();

					require ('Email.php');
					$mail = new Email;
					$body='Your verification code is: '.$loginCode.'.';
					$mail->sendEmail($row['email'], 'Email Verificaiton Code', $body);

					return array('error' => TRUE, 'message' => 'Device verification. ' . $loginCode, 'needVerification' => TRUE);
				}
			}else{
				return array('error' => TRUE, 'message' => 'Wrong password.');
			}
		}

		return array('error' => TRUE, 'message' => 'User does not exists.');
	}

	public function doVerifyDevice($username = NULL, $loginCode = NULL){
		if((!isset($username) || trim($username)===''))
			return array('error' => TRUE, 'message' => 'Username cannot be empty.');

		if((!isset($loginCode) || trim($loginCode)===''))
			return array('error' => TRUE, 'message' => 'Login Code cannot be empty.');

		require ('dbconnect.php');
		$stmt = $conn->prepare("SELECT username FROM app_user WHERE loginCode = ?");
		$stmt->bind_param("s", $loginCode);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();

		if($result->num_rows != 0){
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			if($row['username'] === $username){
				$dateTime = date("Y-m-d H:i:s");

				$stmt = $conn->prepare("UPDATE app_user SET lastIP = ?, loginCode = NULL, updatedTime = ? WHERE username = ?");
				$stmt->bind_param("sss", $_SERVER['REMOTE_ADDR'], $dateTime, $username);
				$stmt->execute();
				$stmt->close();

				return array('error' => FALSE, 'message' => 'Device Verification success.');
			}
		}
		return array('error' => TRUE, 'message' => 'Invalid Login Code.');
	}

	public function doRequestPasswordRecoveryCode($username = NULL){
		if((!isset($username) || trim($username)===''))
			return array('error' => TRUE, 'message' => 'Username cannot be empty.');

		require ('dbconnect.php');
		$stmt = $conn->prepare("SELECT username, email FROM app_user WHERE username = ?");
		$stmt->bind_param("s", $username);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();

		if($result->num_rows != 0){
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

			$status = "r"; // Activated
			$dateTime = date("Y-m-d H:i:s");

			$stmt = $conn->prepare("UPDATE app_user SET status = ?, updatedTime = ? WHERE username = ?");
			$stmt->bind_param("sss", $status, $dateTime, $row['username']);
			$stmt->execute();
			$stmt->close();

			$recoveryCode = substr(md5(mt_rand()),0,15);
			$stmt = $conn->prepare("INSERT INTO app_user_recovery (username, recoveryCode, createdTime) VALUES (?, ?, ?)");
			$stmt->bind_param("sss", $row['username'], $recoveryCode, $dateTime);
			$stmt->execute();
			$stmt->close();

			require ('Email.php');
			$mail = new Email;
			$body='Your password recovery code is: '.$recoveryCode.'<br> To reset your password please click on the following link <a href="http://localhost/project/auth-system/reset_password.php?id='.$row['username'].'&code='.$recoveryCode.'">reset_password.php?id='.$row['username'].'&code='.$recoveryCode.'</a>.';
			$mail->sendEmail($row['email'], 'Password Recovery Code', $body);

			return array('error' => FALSE, 'message' => 'OK. ' . $recoveryCode);
		}
		return array('error' => TRUE, 'message' => 'User does not exists.');
	}

	public function doResetPassword($username = NULL, $recoveryCode = NULL, $newPassword = NULL){
		if((!isset($username) || trim($username)===''))
			return array('error' => TRUE, 'message' => 'Username cannot be empty.');

		if((!isset($recoveryCode) || trim($recoveryCode)===''))
			return array('error' => TRUE, 'message' => 'Recovery Code cannot be empty.');

		if((!isset($newPassword) || trim($newPassword)===''))
			return array('error' => TRUE, 'message' => 'Password cannot be empty.');

		require ('dbconnect.php');
		$stmt = $conn->prepare("SELECT username FROM app_user_recovery WHERE recoveryCode = ?");
		$stmt->bind_param("s", $recoveryCode);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();

		if($result->num_rows != 0){
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			if($row['username'] === $username){
				$status = "t";
				$dateTime = date("Y-m-d H:i:s");

				$stmt = $conn->prepare("UPDATE app_user SET password = ?, status = ?, updatedTime = ? WHERE username = ?");
				$stmt->bind_param("ssss", password_hash($newPassword, PASSWORD_DEFAULT), $status, $dateTime, $username);
				$stmt->execute();
				$stmt->close();

				$stmt = $conn->prepare("DELETE FROM app_user_recovery WHERE username = ? AND recoveryCode = ?");
				$stmt->bind_param("ss", $username, $recoveryCode);
				$stmt->execute();
				$stmt->close();

				return array('error' => FALSE, 'message' => 'Reset Password success, you can login now.');
			}
		}
		return array('error' => TRUE, 'message' => 'Invalid Login Code.');
	}

	public function isLoggedIn(){
		if(!empty(@$_SESSION['user_id'])){   
			return TRUE;        
		}else{
			return FALSE;
		}
	}

	public function doLogout(){
		session_destroy();
		header('Location: login.php');
	}
}