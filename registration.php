<?php
require ('AuthLib.php');
$auth = new AuthLib();
if(isset($_POST['submit'])){
	$user['username'] = $_POST['username'];
	$user['email'] = $_POST['email'];
	$user['password'] = trim($_POST['password']);
	$res = $auth->doCreateUser($user);

	if($res['error'] === FALSE){
		echo "We've sent an email to you.";
	}else {
		echo $res['message'];
	}
}
?>
<form action="registration.php" method="POST">
	<input type="text" name="username">
	<input type="email" name="email">
	<input type="password" name="password">
	<input type="submit" name="submit" value="Registration">
	<br>
	<a href="login.php">Already have an account? Sign in</a>
</form>